/* Colorscheme */
static const char *colorname[] = {
	/* 8 normal colors */
	[0] = "#0e1013", /* black   */
	[1] = "#e55561", /* red     */
	[2] = "#8ebd6b", /* green   */
	[3] = "#e2b86b", /* yellow  */
	[4] = "#4fa6ed", /* blue    */
	[5] = "#bf68d9", /* magenta */
	[6] = "#48b0bd", /* cyan    */
	[7] = "#abb2bf", /* white   */
	/* 8 bright colors */
	[8]  = "#37383d", /* black   */
	[9]  = "#e16d77", /* red     */
	[10] = "#99bc80", /* green   */
	[11] = "#dfbe81", /* yellow  */
	[12] = "#68aee8", /* blue    */
	[13] = "#c27fd7", /* magenta */
	[14] = "#5fafb9", /* cyan    */
	[15] = "#fafafa", /* white   */
	/* special colors */
	[255] = 0,
	[256] = "#1f2329", /* background */
	[257] = "#fafafa", /* foreground */
	[258] = "#ff7700", /* cursor background */
	[259] = "#000000", /* cursor foreground */
};
