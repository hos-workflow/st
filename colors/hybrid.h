/* Colorscheme */
static const char *colorname[] = {
	/* Normal */
	[0] = "#1d1f22",
	[1] = "#8d2e32",
	[2] = "#798431",
	[3] = "#e58a50",
	[4] = "#4b6b88",
	[5] = "#6e5079",
	[6] = "#4d7b74",
	[7] = "#5a626a",
	/* Bright */
	[8]  = "#2a2e33",
	[9]  = "#b84d51",
	[10] = "#b3bf5a",
	[11] = "#e4b55e",
	[12] = "#6e90b0",
	[13] = "#a17eac",
	[14] = "#7fbfb4",
	[15] = "#b5b9b6",
	/* more colors can be added after 255 to use with DefaultXX */
	[255] = 0,
	[256] = "#161719", /* 256 -> bg */
	[257] = "#b7bcba", /* 257 -> fg */
	[258] = "#ff7700", /* 258 -> cursor */
	[259] = "#000000", /* 259 -> rev cursor*/
};
