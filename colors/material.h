/* Colorscheme */
static const char *colorname[] = {
	/* 8 normal colors */
	[0] = "#1e282d", /* black   */
	[1] = "#eb606b", /* red     */
	[2] = "#c3e88d", /* green   */
	[3] = "#f7eb95", /* yellow  */
	[4] = "#80cbc4", /* blue    */
	[5] = "#ff2f90", /* magenta */
	[6] = "#aeddff", /* cyan    */
	[7] = "#ffffff", /* white   */
	/* 8 bright colors */
	[8]  = "#282C34", /* black   */
	[9]  = "#eb606b", /* red     */
	[10] = "#c3e88d", /* green   */
	[11] = "#f7eb95", /* yellow  */
	[12] = "#7dc6bf", /* blue    */
	[13] = "#6c71c4", /* magenta */
	[14] = "#35434d", /* cyan    */
	[15] = "#ffffff", /* white   */
	/* special colors */
	[255] = 0,
	[256] = "#1F2329", /* background */
	[257] = "#c4c7d1", /* foreground */
	[258] = "#ff7700", /* cursor background */
	[259] = "#000000", /* cursor foreground */
};
